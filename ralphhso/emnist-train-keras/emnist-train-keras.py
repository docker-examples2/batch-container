#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python Docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split

import tensorflow as tf

from keras.models import Sequential, load_model
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras import layers
from keras.layers import *
from keras.utils import np_utils
from pathlib import Path

print("data will be extracted... this may take a while...\n")
data_dir = Path("tmp/data/kaggle_emnist").resolve()
import zipfile
with zipfile.ZipFile("/tf/data/kaggle_emnist.zip", 'r') as zip_ref:
    zip_ref.extractall(data_dir.as_posix())

# Input data files are available in the read-only "../input/" directory
# For example, running this (by clicking run or pressing Shift+Enter) will list all files under the input directory


# You can write up to 20GB to the current directory (/kaggle/working/) that gets preserved as output when you create a version using "Save & Run All" 
# You can also write temporary files to /kaggle/temp/, but they won't be saved outside of the current session


# In[ ]:


import os
for dirname, _, filenames in os.walk(data_dir.as_posix()):
    for filename in filenames:
        print(os.path.join(dirname, filename))


# In[ ]:


train_df = pd.read_csv(data_dir.joinpath('emnist-balanced-train.csv'), header=None)
train_df.head()


# In[ ]:


train_df.shape


# ### Split train dataframe into X & y

# In[ ]:


X_train = train_df.loc[:, 1:]
y_train = train_df.loc[:, 0]

X_train.shape, y_train.shape


# In[ ]:


X_train.head()


# In[ ]:


y_train.head()


# ### Create label dictionary

# In[ ]:


label_map = pd.read_csv(data_dir.joinpath('emnist-balanced-mapping.txt'), 
                        delimiter = ' ', 
                        index_col=0, 
                        header=None, 
                        squeeze=True)
label_map.head()


# In[ ]:


label_dictionary = {}
for index, label in enumerate(label_map):
    label_dictionary[index] = chr(label)

label_dictionary


# ### Visualize sample data

# In[ ]:


# Sample entry number 42
sample_image = X_train.iloc[42]
sample_label = y_train.iloc[42]
sample_image.shape, sample_label


# In[ ]:


W = 28
H = 28


# In[ ]:


print("Label entry 42:", label_dictionary[sample_label])
plt.imshow(sample_image.values.reshape(W, H), cmap=plt.cm.gray)
plt.show()


# In[ ]:


def reshape_and_rotate(image):
    W = 28
    H = 28
    image = image.reshape(W, H)
    image = np.fliplr(image)
    image = np.rot90(image)
    return image

print("Label entry 42:", label_dictionary[sample_label])
plt.imshow(reshape_and_rotate(sample_image.values), cmap=plt.cm.gray)
plt.show()


# Next we want to apply reshape_and_rotate to all images in X_train

# In[ ]:


# note: np.apply_along_axis returns a numpy array, X_train is not a pandas.DataFrame anymore
X_train = np.apply_along_axis(reshape_and_rotate, 1, X_train.values)
X_train.shape


# ## Visualize more sample

# In[ ]:


sample_image = X_train[42]
sample_label = y_train.iloc[42]
print("Label entry 42:", label_dictionary[sample_label])
plt.imshow(sample_image.reshape(W, H), cmap=plt.cm.gray)
plt.show()


for i in range(100, 106):
    plt.subplot(390 + (i+1))
    plt.imshow(X_train[i], cmap=plt.cm.gray)
    plt.title(label_dictionary[y_train[i]])


# Looks good
# 
# ### Normalize Data

# In[ ]:


X_train = X_train.astype('float32') / 255


# ### One Hot Encode Label

# In[ ]:


number_of_classes = y_train.nunique()
number_of_classes


# In[ ]:


y_train = np_utils.to_categorical(y_train, number_of_classes)
y_train.shape


# In[ ]:


y_train


# In[ ]:


# Reshape to fit model input shape
# Tensorflow (batch, width, height, channels)
X_train = X_train.reshape(-1, W, H, 1)
X_train.shape


# In[ ]:


# Split 10% validation 
X_train, X_val, y_train, y_val = train_test_split(X_train, 
                                                  y_train, 
                                                  test_size= 0.1, 
                                                  random_state=88)


# ### Define model
# LeNet 5

# In[ ]:


model = Sequential()

model.add(layers.Conv2D(filters=32, kernel_size=(5,5), padding='same', activation='relu', input_shape=(W, H, 1)))
model.add(layers.MaxPool2D(strides=2))
model.add(layers.Conv2D(filters=48, kernel_size=(5,5), padding='valid', activation='relu'))
model.add(layers.MaxPool2D(strides=2))
model.add(layers.Flatten())
model.add(layers.Dense(256, activation='relu'))
model.add(layers.Dense(84, activation='relu'))
model.add(layers.Dense(number_of_classes, activation='softmax'))

model.summary()


# In[ ]:


optimizer_name = 'adam'

model.compile(loss='categorical_crossentropy', optimizer=optimizer_name, metrics=['accuracy'])

early_stopping = EarlyStopping(monitor='val_loss', patience=5, verbose=1, mode='min')
mcp_save = ModelCheckpoint('my_model.h5', save_best_only=True, monitor='val_loss', verbose=1, mode='auto')


# In[ ]:


history = model.fit(X_train,
                    y_train, 
                    epochs=30, 
                    batch_size=32, 
                    verbose=1, 
                    validation_split=0.1,
                    callbacks=[early_stopping, mcp_save])


# In[ ]:


# plot accuracy and loss
def plotgraph(epochs, acc, val_acc):
    # Plot training & validation accuracy values
    plt.plot(epochs, acc, 'b')
    plt.plot(epochs, val_acc, 'r')
    plt.title('Model accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.legend(['Train', 'Val'], loc='upper left')
    plt.show()
    
acc = history.history['accuracy']
val_acc = history.history['val_accuracy']
loss = history.history['loss']
val_loss = history.history['val_loss']
epochs = range(1,len(acc)+1)


# In[ ]:


# Accuracy curve
plotgraph(epochs, acc, val_acc)


# In[ ]:


# loss curve
plotgraph(epochs, loss, val_loss)


# ### Test model

# In[ ]:


# Load best model
model = load_model('/tf/my_model.h5')
model.summary()


# In[ ]:


y_pred = model.predict(X_val)
y_pred.shape


# In[ ]:


for i in range(10, 16):
    plt.subplot(380 + (i%10+1))
    plt.imshow(X_val[i].reshape(28, 28), cmap=plt.cm.gray)
    plt.title(label_dictionary[y_pred[i].argmax()])


# In[ ]:


for i in range(42, 48):
    plt.subplot(380 + (i%10+1))
    plt.imshow(X_val[i].reshape(28, 28), cmap=plt.cm.gray)
    plt.title(label_dictionary[y_pred[i].argmax()])


# #### Test accuracy

# In[ ]:


model.evaluate(X_val, y_val)


# ### Load test dataset & preprocess image like how we did to train dataset

# In[ ]:


test_df = pd.read_csv(data_dir.joinpath('emnist-balanced-test.csv'), header=None)
test_df.shape


# In[ ]:


test_df.describe()


# In[ ]:


X_test = test_df.loc[:, 1:]
y_test = test_df.loc[:, 0]

X_test.shape, y_test.shape


# In[ ]:


X_test = np.apply_along_axis(reshape_and_rotate, 1, X_test.values)
y_test = np_utils.to_categorical(y_test, number_of_classes)

X_test.shape, y_test.shape


# In[ ]:


X_test = X_test.astype('float32') / 255


# In[ ]:


X_test = X_test.reshape(-1, W, H, 1)
X_test.shape


# In[ ]:


model.evaluate(X_test, y_test)


# In[ ]:




